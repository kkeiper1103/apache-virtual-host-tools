#!/bin/bash

if [ "$(whoami)" != "root" ]; then
	echo "This Script Needs Root Privileges."
	exit 11
fi

if [ $# -eq 1 ]
then
	
	if [ ! -d ~/websites/$1 ]; then
		echo "ABORT: There Is No Site Named That In Your Websites Folder."
		exit 12
	fi
	
	rm -Rf ~/websites/$1
	
	sed -i "s/127.0.0.1		$1.local	www.$1.local//g" /etc/hosts
	
	a2dissite $1
	
	rm /etc/apache2/sites-available/$1
	
	service apache2 reload

else
	
	echo "Usage: $0 sitename"
	exit 10
	
fi


