#!/bin/bash

if [ "$(whoami)" != "root" ]; then
	echo "This Script Needs Root Privileges."
	exit 11
fi

if [ $# -eq 1 ]
then
	
	if [ -d ~/websites/$1 ]; then
		echo "ABORT: DocRoot already exists!"
		exit 12
	fi
	
	mkdir ~/websites/$1
	

	wget http://wordpress.org/latest.zip -O ~/websites/$1/latest.zip #get latest wordpress version and download to the given directory (-O flag)
	
	unzip ~/websites/$1/latest.zip -d ~/websites/$1/ # unzip downloaded file in that directory (-d flag says where to unzip to)
	
	mv ~/websites/$1/wordpress/* ~/websites/$1/ # the unzip adds a directory called "wordpress", so move everything in there one level up
	
	rm -Rf ~/websites/$1/wordpress # remove the wordpress directory
	
	
	# create database for wordpress site
	
	echo "Create Database for this site? (y/N)"
	read create_db
	
	if [ ("$create_db" != "y") || ("$create_db" != "Y") ]; then
	
		SQL=`printf "CREATE DATABASE %s" $1`
	
		mysql -u $(whoami) -p -e $SQL
		
		echo "Create Database Specific User? (Y/n)"
		read create_user_for_db
		
		if [ ("$create_user_for_db" != "N") || ("$create_user_for_db" != "n") ]; then
		
			echo "Enter Password For New Database User"
			read password
			
			SQL=`printf "CREATE USER '%s'@'localhost' IDENTIFIED BY '%s';GRANT ALL PRIVILEGES ON %s.* to '%s'@'localhost';" $1 $password $1 $1`
			
			mysql -u $(whoami) -p -e $SQL
			
		fi
	
	fi
	
	# end create database
	

	chown -Rf kyle:www-data	~/websites/$1

	cat > /etc/apache2/sites-available/$1 <<endOfVhost
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	
	ServerName $1.local
	ServerAlias www.$1.local	

	DocumentRoot /home/kyle/websites/$1
	
	RewriteEngine On
	
	<Directory />
		Options FollowSymLinks
		AllowOverride All
	</Directory>
	<Directory /home/kyle/websites/$1/>
		
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>

	ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
	<Directory "/usr/lib/cgi-bin">
		AllowOverride None
		Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
		Order allow,deny
		Allow from all
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn

	CustomLog ${APACHE_LOG_DIR}/access.log combined

	Alias /doc/ "/usr/share/doc/"
	<Directory "/usr/share/doc/">
		Options Indexes MultiViews FollowSymLinks
		AllowOverride None
		Order deny,allow
		Deny from all
		Allow from 127.0.0.0/255.0.0.0 ::1/128
	</Directory>

</VirtualHost>
endOfVhost

	echo "127.0.0.1		$1.local	www.$1.local" >> /etc/hosts

	a2ensite $1

	service apache2 reload

else
	
	echo "Usage: $0 sitename"
	exit 10
	
fi


