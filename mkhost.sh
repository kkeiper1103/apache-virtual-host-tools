#!/bin/bash

if [ "$(whoami)" != "root" ]; then
	echo "ABORT: This Script Needs Root Privileges."
	exit 11
fi

if [ $# -eq 1 ]
then
	
	# figure out which distro we're dealing with
	distro=$(lsb_release -si)
	
	#variable for vhost name
	vhost_folder=$1.com
	vhost_decl=$1.com
	
	if [ -d /var/www/vhosts/$vhost_folder ]; then
		echo "ABORT: DocRoot already exists!"
		exit 12
	fi
	
	echo "Creating docroot in /var/www/vhosts/$vhost_folder ..."
	
	mkdir /var/www/vhosts/$vhost_folder
	mkdir /var/www/vhosts/$vhost_folder/logs
	mkdir /var/www/vhosts/$vhost_folder/public
	
	echo "Writing index.html in the ./public folder ..."
	
	cat > /var/www/vhosts/$vhost_folder/public/index.html <<endHTML
<!DOCTYPE html>
<html>
	<head>
		<title>$1</title>
	</head>
	<body>
		<h2>It Worked!</h2>
	</body>
</html>
endHTML

  echo "Fixing Permission Issues from running via sudo ..."
  
  case "$distro" in
    "Ubuntu") user="www-data";;
    "CentOS") user="apache";;
  esac
  
	chown -Rf $user:wrl_users /var/www/vhosts/$vhost_folder
	chmod g+w /var/www/vhosts/$vhost_folder -Rf

  echo "Writing Virtual Host Definition in /etc/httpd/vhost.d/$vhost_decl ..."

  case "$distro" in
    "Ubuntu") location="/etc/apache2/sites-available";;
    "CentOS") location="/etc/httpd/vhost.d";;
  esac
  
	cat > $location/$vhost_decl <<endOfVhost
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	
	ServerName $vhost_folder

	DocumentRoot /var/www/vhosts/$vhost_folder/public
	
	RewriteEngine On
	
	<Directory />
		Options FollowSymLinks
		AllowOverride All
	</Directory>
	
	<Directory /var/www/vhosts/$vhost_folder/public/>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>

	ErrorLog /var/www/vhosts/$vhost_folder/logs/error.log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn

	CustomLog /var/www/vhosts/$vhost_folder/logs/access.log combined

</VirtualHost>

endOfVhost
  
  # configtest prints to stderr (2), so redirect output to stdout (1)
  IS_CONF_OKAY=$(apachectl configtest 2>&1)
  
  if [[ $IS_CONF_OKAY =~ Syntax\ OK ]]; then
    echo "Syntax Okay. Reloading Apache2 ..."
    
    case "$distro" in
      "Ubuntu") a2ensite $vhost_decl; service apache2 reload;;
      "CentOS") service httpd reload;;
    esac
    
    echo "Finished With Creating and Enabling $vhost_decl ."
    echo "Don't forget to set the DNS, if necessary"
    
  else
    echo "There is an error with the default vhost. Please correct and then reload Apache2 via 'service httpd reload'"
  fi

else
	
	echo "Usage: $0 sitename"
	exit 10
	
fi


